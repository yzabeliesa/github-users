package com.yzabeliesa.githubusers.data.api

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.data.util.FAKE_USERS_LIST
import com.yzabeliesa.githubusers.util.Error
import com.yzabeliesa.githubusers.util.Status
import junitparams.JUnitParamsRunner
import junitparams.Parameters
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.spy
import org.mockito.MockitoAnnotations
import retrofit2.Response

@RunWith(JUnitParamsRunner::class)
class RemoteUserDataSourceTest {

    @Mock
    private lateinit var mockApiService: GithubApiService

    @Mock
    private lateinit var mockErrorHandler: RemoteUserErrorHandler

    private lateinit var remoteUserDataSource: RemoteUserDataSource

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        remoteUserDataSource = spy(RemoteUserDataSource(mockApiService, mockErrorHandler))
    }

    @Test
    fun `getUsers success`() = runBlocking {
        val fakeUsersResponse = FAKE_USERS_LIST
        val mockResponse = Response.success(fakeUsersResponse)
        whenever(mockApiService.getUsers(anyInt())).thenReturn(mockResponse)

        val result = remoteUserDataSource.getUsers(anyInt())
        assert(result.status == Status.SUCCESS)
        assert(result.value == fakeUsersResponse)
    }

    @Test
    @Parameters("401", "402", "403", "404", "502", "503")
    fun `getUsers API error`(code: Int) = runBlockingTest {
        val errorBody = "{}".toResponseBody()
        val mockResponse = Response.error<List<User>>(code, errorBody)
        whenever(mockApiService.getUsers(anyInt()))
            .thenReturn(mockResponse)

        whenever(mockErrorHandler.getError(anyInt()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteUserDataSource.getUsers(anyInt())
        assert(result.status == Status.ERROR)
    }

    @Test
    @Parameters("401", "402", "403", "404", "502", "503")
    fun `getUsers API error retry`(code: Int) = runBlockingTest {
        val errorBody = "{}".toResponseBody()
        val mockResponse = Response.error<List<User>>(code, errorBody)
        whenever(mockApiService.getUsers(anyInt()))
            .thenReturn(mockResponse)

        whenever(mockErrorHandler.getError(anyInt()))
            .thenReturn(Error.GENERIC_ERROR)

        remoteUserDataSource.getUsers(anyInt())
        verify(mockApiService, times(RETRIES + 1)).getUsers(anyInt())
    }

    @Test
    fun `getUsers exception`() = runBlockingTest {
        whenever(mockApiService.getUsers(anyInt())).then {
            throw Exception("Some exception")
        }
        whenever(mockErrorHandler.getError(any<Exception>()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteUserDataSource.getUsers(anyInt())
        assert(result.status == Status.ERROR)
        assert(result.error == Error.GENERIC_ERROR)
    }

    @Test
    fun `getUsers exception retry`() = runBlockingTest {
        whenever(mockApiService.getUsers(anyInt())).then {
            throw Exception("Some exception")
        }
        whenever(mockErrorHandler.getError(any<Exception>()))
            .thenReturn(Error.GENERIC_ERROR)

        remoteUserDataSource.getUsers(anyInt())
        verify(mockApiService, times(RETRIES + 1)).getUsers(anyInt())
    }

    @Test
    fun `getUser success`() = runBlocking {
        val username = FAKE_USERS_LIST[0].username ?: ""
        val fakeUsersResponse = FAKE_USERS_LIST[0]
        val mockResponse = Response.success(fakeUsersResponse)
        whenever(mockApiService.getUser(anyString()))
            .thenReturn(mockResponse)

        val result = remoteUserDataSource.getUser(username)
        assert(result.status == Status.SUCCESS)
        assert(result.value == fakeUsersResponse)
    }

    @Test
    @Parameters("401", "402", "403", "404", "502", "503")
    fun `getUser API error`(code: Int) = runBlockingTest {
        val username = FAKE_USERS_LIST[0].username ?: ""
        val errorBody = "{}".toResponseBody()
        val mockResponse = Response.error<User>(code, errorBody)
        whenever(mockApiService.getUser(anyString()))
            .thenReturn(mockResponse)

        whenever(mockErrorHandler.getError(anyInt()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteUserDataSource.getUser(username)
        assert(result.status == Status.ERROR)
        assert(result.error == Error.GENERIC_ERROR)
    }

    @Test
    @Parameters("401", "402", "403", "404", "502", "503")
    fun `getUser API error retry`(code: Int) = runBlockingTest {
        val username = FAKE_USERS_LIST[0].username ?: ""
        val errorBody = "{}".toResponseBody()
        val mockResponse = Response.error<User>(code, errorBody)
        whenever(mockApiService.getUser(anyString()))
            .thenReturn(mockResponse)

        whenever(mockErrorHandler.getError(anyInt()))
            .thenReturn(Error.GENERIC_ERROR)

        remoteUserDataSource.getUser(username)
        verify(mockApiService, times(RETRIES + 1)).getUser(anyString())
    }

    @Test
    fun `getUser exception`() = runBlockingTest {
        val username = FAKE_USERS_LIST[0].username ?: ""
        whenever(mockApiService.getUser(anyString())).then {
            throw Exception("Some exception")
        }
        whenever(mockErrorHandler.getError(any<Exception>()))
            .thenReturn(Error.GENERIC_ERROR)

        val result = remoteUserDataSource.getUser(username)
        assert(result.status == Status.ERROR)
        assert(result.error == Error.GENERIC_ERROR)
    }

    @Test
    fun `getUser exception retry`() = runBlockingTest {
        val username = FAKE_USERS_LIST[0].username ?: ""
        whenever(mockApiService.getUser(anyString())).then {
            throw Exception("Some exception")
        }
        whenever(mockErrorHandler.getError(any<Exception>()))
            .thenReturn(Error.GENERIC_ERROR)

        remoteUserDataSource.getUser(username)
        verify(mockApiService, times(RETRIES + 1)).getUser(anyString())
    }

}