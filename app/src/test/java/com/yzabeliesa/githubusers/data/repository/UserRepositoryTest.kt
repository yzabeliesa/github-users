package com.yzabeliesa.githubusers.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.*
import com.yzabeliesa.githubusers.data.api.RemoteUserDataSource
import com.yzabeliesa.githubusers.data.db.LocalUserDataSource
import com.yzabeliesa.githubusers.data.model.Data
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.data.prefs.LocalConfigDataSource
import com.yzabeliesa.githubusers.data.util.FAKE_USERS_LIST
import com.yzabeliesa.githubusers.util.Error
import com.yzabeliesa.githubusers.util.Status
import junitparams.JUnitParamsRunner
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(JUnitParamsRunner::class)
class UserRepositoryTest {

    @Rule
    @JvmField val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockLocalUserDataSource: LocalUserDataSource

    @Mock
    private lateinit var mockRemoteUserDataSource: RemoteUserDataSource

    @Mock
    private lateinit var mockLocalConfigDataSource: LocalConfigDataSource

    private lateinit var userRepository: UserRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        userRepository = spy(
            UserRepository(
            mockRemoteUserDataSource,
            mockLocalUserDataSource,
            mockLocalConfigDataSource,
            Dispatchers.Unconfined)
        )
        Dispatchers.setMain(Dispatchers.Unconfined)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `getUsers success`() = runBlocking {
        val usersList = FAKE_USERS_LIST
        val response = Data.success(usersList)
        whenever(mockRemoteUserDataSource.getUsers(anyInt())).thenReturn(response)
        whenever(mockLocalUserDataSource.getAllUsers()).thenReturn(MutableLiveData(usersList))

        val result = userRepository.getUsers(anyInt())
        result.observeForever {  }

        assert(result.value?.status == Status.SUCCESS)
        assert(result.value == response)
        verify(mockRemoteUserDataSource).getUsers(anyInt())
        verify(mockLocalUserDataSource).insertUsers(usersList)
        verify(mockLocalConfigDataSource).setPageSize(usersList.size)

        return@runBlocking
    }

    @Test
    fun `getUsers verify prevent null overwrite`() = runBlocking {
        val usersList = FAKE_USERS_LIST
        val newUsersList = FAKE_USERS_LIST.map { user -> user.copy(name = null) }
        val response = Data.success(newUsersList)
        whenever(mockRemoteUserDataSource.getUsers(anyInt())).thenReturn(response)
        whenever(mockLocalUserDataSource.getAllUsers()).thenReturn(MutableLiveData(usersList))

        val result = userRepository.getUsers(anyInt())
        result.observeForever {  }

        val captor = argumentCaptor<List<User>>()
        verify(mockLocalUserDataSource).insertUsers(captor.capture())
        captor.firstValue.forEach { user ->
            assert(user.name != null)
        }

        return@runBlocking
    }

    @Test
    fun `getUsers failed`() = runBlocking {
        val usersList = FAKE_USERS_LIST
        val response = Data.error<List<User>>(Error.GENERIC_ERROR)
        whenever(mockRemoteUserDataSource.getUsers(anyInt())).thenReturn(response)
        whenever(mockLocalUserDataSource.getAllUsers()).thenReturn(MutableLiveData(usersList))

        val result = userRepository.getUsers(anyInt())
        result.observeForever {  }

        assert(result.value?.status == Status.ERROR)
        verify(mockLocalUserDataSource, times(0)).insertUsers(usersList)
    }

    @Test
    fun `getUserDetails success`() = runBlocking {
        val user = FAKE_USERS_LIST[0]
        val response = Data.success(user)
        whenever(mockRemoteUserDataSource.getUser(anyString())).thenReturn(response)
        whenever(mockLocalUserDataSource.getUser(anyString())).thenReturn(MutableLiveData(user))

        val result = userRepository.getUserDetails(anyString())
        result.observeForever {  }

        assert(result.value?.status == Status.SUCCESS)
        assert(result.value == response)
        verify(mockRemoteUserDataSource).getUser(anyString())
        verify(mockLocalUserDataSource).insertUser(user)
    }

    @Test
    fun `getUserDetails verify prevent overwrite`() = runBlocking {
        val user = FAKE_USERS_LIST[0]
        val newUser = user.copy(name = null)
        val response = Data.success(newUser)
        whenever(mockRemoteUserDataSource.getUser(anyString())).thenReturn(response)
        whenever(mockLocalUserDataSource.getUser(anyString())).thenReturn(MutableLiveData(user))

        val result = userRepository.getUserDetails(anyString())
        result.observeForever {  }

        val captor = argumentCaptor<User>()
        verify(mockLocalUserDataSource).insertUser(captor.capture())
        assert(captor.firstValue.name != null)
    }

    @Test
    fun `getUserDetails failed`() = runBlocking {
        val user = FAKE_USERS_LIST[0]
        val response = Data.error<User>(Error.GENERIC_ERROR)
        whenever(mockRemoteUserDataSource.getUser(anyString())).thenReturn(response)
        whenever(mockLocalUserDataSource.getUser(anyString())).thenReturn(MutableLiveData(user))

        val result = userRepository.getUserDetails(anyString())
        result.observeForever {  }

        assert(result.value?.status == Status.ERROR)
        verify(mockLocalUserDataSource, times(0)).insertUser(user)
    }

    @Test
    fun saveNote() = runBlocking {
        val user = FAKE_USERS_LIST[0].copy(note = "old note")
        val note = "new note"

        userRepository.saveUserNote(user, note)
        val captor = argumentCaptor<User>()
        verify(mockLocalUserDataSource).insertUser(captor.capture())
        assert(captor.firstValue.note == note)
    }

}