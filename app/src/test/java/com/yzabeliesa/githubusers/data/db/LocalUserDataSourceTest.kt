package com.yzabeliesa.githubusers.data.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.yzabeliesa.githubusers.data.util.FAKE_USERS_LIST
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LocalUserDataSourceTest {

    @Rule
    @JvmField val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockUsersDao: GithubUsersDao

    private lateinit var localUserDataSource: LocalUserDataSource

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        localUserDataSource = spy(LocalUserDataSource(mockUsersDao))
    }

    @Test
    fun getAllUsers() {
        val fakeListLiveData = MutableLiveData(FAKE_USERS_LIST)
        whenever(mockUsersDao.getAllUsers()).thenReturn(fakeListLiveData)

        val result = localUserDataSource.getAllUsers()
        result.observeForever { }
        assert(result.value == fakeListLiveData.value)
    }

    @Test
    fun getUserByUsername() {
        val fakeUserLiveData = MutableLiveData(FAKE_USERS_LIST[0])
        whenever(mockUsersDao.getUser(anyString())).thenReturn(fakeUserLiveData)

        val result = localUserDataSource.getUser("username")
        result.observeForever { }
        assert(result.value == fakeUserLiveData.value)
    }

    @Test
    fun insertUsers() = runBlocking {
        val users = FAKE_USERS_LIST
        localUserDataSource.insertUsers(users)
        verify(mockUsersDao).insertUsers(users)
    }

    @Test
    fun insertUser() = runBlocking {
        val user = FAKE_USERS_LIST[0]
        localUserDataSource.insertUser(user)
        verify(mockUsersDao).insertUser(user)
    }

}