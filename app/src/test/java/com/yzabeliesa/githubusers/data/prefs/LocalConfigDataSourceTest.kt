package com.yzabeliesa.githubusers.data.prefs

import android.content.SharedPreferences
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test

import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class LocalConfigDataSourceTest {

    @Mock
    private lateinit var mockSharedPrefs: SharedPreferences

    @Mock
    private lateinit var mockSharedPrefsEditor: SharedPreferences.Editor

    private lateinit var localConfigDataSource: LocalConfigDataSource

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        localConfigDataSource = LocalConfigDataSource(mockSharedPrefs)
    }

    @Test
    fun getPageSize() {
        val pageSize = 10
        whenever(mockSharedPrefs.getInt(eq(LocalConfigDataSource.PAGE_SIZE_KEY), anyInt()))
            .thenReturn(pageSize)

        val result = localConfigDataSource.getPageSize()
        assert(result == pageSize)
    }

    @Test
    fun setPageSize() {
        whenever(mockSharedPrefs.edit()).thenReturn(mockSharedPrefsEditor)
        whenever(mockSharedPrefsEditor.putInt(
            eq(LocalConfigDataSource.PAGE_SIZE_KEY),
            anyInt()
        )).thenReturn(mockSharedPrefsEditor)
        whenever(mockSharedPrefsEditor.commit()).thenReturn(true)

        val result = localConfigDataSource.setPageSize(10)
        assert(result)
    }

}