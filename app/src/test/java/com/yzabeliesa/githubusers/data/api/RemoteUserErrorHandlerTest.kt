package com.yzabeliesa.githubusers.data.api

import com.yzabeliesa.githubusers.util.Error
import junitparams.JUnitParamsRunner
import junitparams.Parameters
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.lang.Exception

@RunWith(JUnitParamsRunner::class)
class RemoteUserErrorHandlerTest {

    private lateinit var remoteUserErrorHandler: RemoteUserErrorHandler

    @Before
    fun setUp() {
        remoteUserErrorHandler = RemoteUserErrorHandler()
    }

    @Test
    @Parameters("400,BAD_REQUEST",
        "401,UNAUTHORIZED",
        "402,GENERIC_ERROR",
        "403,FORBIDDEN",
        "404,NOT_FOUND",
        "422,UNPROCESSABLE_ENTITY",
        "500,GENERIC_ERROR",
        "502,GENERIC_ERROR",
        "503,GENERIC_ERROR")
    fun `get error from code`(code: Int, expected: String) {
        val error = remoteUserErrorHandler.getError(code)
        assert(error == Error.valueOf(expected))
    }

    @Test
    fun `get error from IOException`() {
        val error = remoteUserErrorHandler.getError(IOException())
        assert(error == Error.NO_INTERNET_CONNECTION)
    }

    @Test
    fun `get error from any other exception`() {
        val error = remoteUserErrorHandler.getError(Exception("Not IO"))
        assert(error == Error.GENERIC_ERROR)
    }

}