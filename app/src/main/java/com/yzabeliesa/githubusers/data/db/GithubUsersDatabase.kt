package com.yzabeliesa.githubusers.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.yzabeliesa.githubusers.data.model.User

/**
 * Room database.
 */

@Database(entities = [User::class], version = 1, exportSchema = false)
abstract class GithubUsersDatabase : RoomDatabase() {
    abstract fun usersDao(): GithubUsersDao
}