package com.yzabeliesa.githubusers.data.prefs

import android.content.SharedPreferences
import javax.inject.Inject

/**
 * This class handles all config-related actions. Uses shared prefs for saving and retrieving
 * app-wide configs.
 */

class LocalConfigDataSource @Inject constructor (
    private val sharedPreferences: SharedPreferences
) {

    /**
     * Obtains the number of elements in a page.
     */
    fun getPageSize(): Int {
        return sharedPreferences.getInt(PAGE_SIZE_KEY, -1)
    }

    /**
     * Saves the number of elements in a page.
     */
    fun setPageSize(pageSize: Int): Boolean {
        return sharedPreferences
            .edit()
            .putInt(PAGE_SIZE_KEY, pageSize)
            .commit()
    }

    companion object {
        const val PAGE_SIZE_KEY = "page_size"
    }

}