package com.yzabeliesa.githubusers.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Model representing a Github user. The user data is saved to a Room database.
 */

@Entity(tableName = "users")
data class User (

    @PrimaryKey
    val id: Int? = null,

    @SerializedName("login")
    val username: String? = null,

    @SerializedName("avatar_url")
    val avatarUrl: String? = null,

    val type: String? = null,

    val name: String? = null,

    val company: String? = null,

    val blog: String? = null,

    val location: String? = null,

    val email: String? = null,

    val bio: String? = null,

    @SerializedName("public_repos")
    val publicRepos: Long? = null,

    @SerializedName("public_gists")
    val publicGists: Long? = null,

    @SerializedName("followers")
    val followers: Long? = null,

    @SerializedName("following")
    val following: Long? = null,

    @SerializedName("created_at")
    val createdAt: String? = null,

    val note: String? = ""

)