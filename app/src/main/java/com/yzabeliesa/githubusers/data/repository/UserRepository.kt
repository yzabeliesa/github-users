package com.yzabeliesa.githubusers.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.yzabeliesa.githubusers.data.api.RemoteUserDataSource
import com.yzabeliesa.githubusers.data.db.LocalUserDataSource
import com.yzabeliesa.githubusers.data.model.Data
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.data.prefs.LocalConfigDataSource
import com.yzabeliesa.githubusers.util.Status
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * This class manages Github user data from all data sources. In particular, the user repository
 * has these responsibilities:
 * 1. Retrieving data from local data source.
 * 2. Retrieving data from remote data source.
 * 3. Syncing data between remote data source and local data source.
 *
 * @param remoteUserDataSource Remote Github user data source.
 * @param localUserDataSource Local Github user data source.
 * @param localConfigDataSource App-wide config data source.
 * @param coroutineDispatcher Dispatcher for all coroutines launched by the repository.
 */

class UserRepository @Inject constructor (
    private val remoteUserDataSource: RemoteUserDataSource,
    private val localUserDataSource: LocalUserDataSource,
    private val localConfigDataSource: LocalConfigDataSource,
    private val coroutineDispatcher: CoroutineDispatcher
) {

    /**
     * Fetches a list of users after the last retrieved user and saves it to the local source. The
     * number of items per pages are also saved to the local config.
     *
     * @param since User ID of last retrieved user.
     * @return Observable @[LiveData] object containing list of users.
     */
    fun getUsers(since: Int): LiveData<Data<List<User>>> {
        return fetchData(
            { localUserDataSource.getAllUsers() },
            { remoteUserDataSource.getUsers(since) },
            { oldData, newData ->
                // Save page size
                localConfigDataSource.setPageSize(newData.size)

                // Insert data
                val newUsers = newData.map { user ->
                    val oldUser = oldData?.find { it.id == user.id }
                    val newUser = preventNullDataOverwrite(oldUser, user)
                    newUser
                }
                localUserDataSource.insertUsers(newUsers)
            }
        )
    }

    /**
     * Fetches the details of the user with the given username and saves it to the local source.
     *
     * @param username Username of the Github user.
     * @return Observable @[LiveData] object containing user details.
     */
    fun getUserDetails(username: String): LiveData<Data<User>> {
        return fetchData(
            { localUserDataSource.getUser(username) },
            { remoteUserDataSource.getUser(username) },
            { oldData, newData ->
                val newUser = preventNullDataOverwrite(oldData, newData)
                localUserDataSource.insertUser(newUser)
            }
        )
    }

    /**
     * Saves a user's note locally.
     *
     * @param user The user whose note will be saved.
     * @param note The note contents to be saved.
     */
    suspend fun saveUserNote(user: User, note: String) {
        withContext(coroutineDispatcher) {
            val newUser = user.copy(note = note)
            localUserDataSource.insertUser(newUser)
        }
    }

    /**
     * Handles the syncing of fetched data from local and remote sources. The data is first fetched
     * locally and emitted. Data from the remote source is then fetched and saved locally.
     *
     * @param fetchLocal Fetches the data from a local source and returns it as an observable
     * @[LiveData] object.
     * @param fetchRemote Fetches the data from a remote source and returns it as a @[Data] object.
     * @param saveFetchedData The operation performed to save data from the remote source to the
     * local source.
     */
    private fun<T> fetchData (
        fetchLocal: () -> LiveData<T>,
        fetchRemote: suspend () -> Data<T>,
        saveFetchedData: suspend (oldData: T?, newData: T) -> Unit
    ): LiveData<Data<T>> = liveData(coroutineDispatcher) {
        emit(Data.loading<T>())

        // Fetch data from local source
        val localData = fetchLocal()
        emitSource(localData.map { Data.loading(it) })

        // Fetch data from remote source
        val remoteData = fetchRemote()
        if (remoteData.status == Status.SUCCESS) {
            // Save fetched data and emit status
            remoteData.value?.let { data: T ->
                saveFetchedData(localData.value, data)
            }
            // Emit success data
            emitSource(localData.map { Data.success(it) })
        } else emit(remoteData) // Emit error
    }

    /**
     * Prevents data loss upon fetching new user data from a remote source. This method prevents
     * setting non-null data values to null, in which case the old non-null values are retained.
     *
     * @param old User data that was saved locally prior to remote fetch.
     * @param new User data fetched from remote source.
     */
    private fun preventNullDataOverwrite(old: User?, new: User): User {
        var user = new
        old?.let {
            user = new.copy(
                name = new.name ?: it.name,
                company = new.company ?: it.company,
                blog = new.blog ?: it.blog,
                location = new.location ?: it.location,
                email = new.email ?: it.email,
                bio = new.bio ?: it.bio,
                publicRepos = new.publicRepos ?: it.publicRepos,
                publicGists = new.publicGists ?: it.publicGists,
                followers = new.followers ?: it.followers,
                following = new.following ?: it.following,
                createdAt = new.createdAt ?: it.createdAt,
                note = it.note
            )
        }
        return user
    }

}