package com.yzabeliesa.githubusers.data.api

import com.yzabeliesa.githubusers.data.model.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * This class defines the API endpoints for obtaining Github user data.
 */

interface GithubApiService {

    @GET("/users")
    suspend fun getUsers(@Query("since") since: Int = 0): Response<List<User>>

    @GET("/users/{username}")
    suspend fun getUser(@Path("username") username: String): Response<User>

}