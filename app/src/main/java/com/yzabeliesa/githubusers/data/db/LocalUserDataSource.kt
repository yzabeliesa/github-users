package com.yzabeliesa.githubusers.data.db

import androidx.lifecycle.LiveData
import com.yzabeliesa.githubusers.data.model.User
import javax.inject.Inject

/**
 * This class is responsible for obtaining from and persisting Github user-related data to a local
 * source (i.e. a Room database).
 *
 * @param usersDao Github users DAO for performing Room database operations.
 */

class LocalUserDataSource @Inject constructor (
    private val usersDao: GithubUsersDao
) {

    /**
     * Obtains all saved Github users.
     */
    fun getAllUsers(): LiveData<List<User>> { return usersDao.getAllUsers() }

    /**
     * Obtains a saved user by the username.
     */
    fun getUser(username: String): LiveData<User> { return usersDao.getUser(username) }

    /**
     * Saves a list of users to the database. If user to be inserted already exists in the database,
     * the data will be replaced.
     */
    suspend fun insertUsers(users: List<User>) { usersDao.insertUsers(users) }

    /**
     * Saves a user to the database. If user to be inserted already exists in the database, the
     * data will be replaced.
     */
    suspend fun insertUser(user: User) { usersDao.insertUser(user) }

}