package com.yzabeliesa.githubusers.data.api

import com.yzabeliesa.githubusers.data.model.Data
import com.yzabeliesa.githubusers.data.model.User
import kotlinx.coroutines.delay
import retrofit2.Response
import javax.inject.Inject

/**
 * This class is responsible for handling Github user-related data from a remote source
 * (i.e. a Retrofit service).
 */

const val RETRIES = 5
const val INITIAL_RETRY_DELAY = 500L

class RemoteUserDataSource @Inject constructor (
    private val apiService: GithubApiService,
    private val errorHandler: RemoteUserErrorHandler
) {

    /**
     * Obtains a portion of the list of users from the Github API service after the last retrieved
     * user.
     *
     * @param since User ID of last retrieved user.
     * @return List of users wrapped in a @[Data] object.
     */
    suspend fun getUsers(since: Int): Data<List<User>> =
        executeRequest(RETRIES, INITIAL_RETRY_DELAY) { apiService.getUsers(since) }

    /**
     * Obtains the profile of the user with the provided username.
     *
     * @param username Username of the Github user.
     * @return User details wrapped in a @[Data] object.
     */
    suspend fun getUser(username: String): Data<User> =
        executeRequest(RETRIES, INITIAL_RETRY_DELAY) { apiService.getUser(username) }

    /**
     * Executes the request and wraps the result in a @[Data] object. In case of an error, the
     * request is retried with an exponential backoff policy.
     *
     * @param tries The number of retries to execute if the request fails.
     * @param retryDelay The length of time to wait before retrying in milliseconds.
     * @param request The request to execute.
     * @return Response wrapped in a @[Data] object.
     */
    private suspend fun <T> executeRequest(
        tries: Int,
        retryDelay: Long,
        request: suspend () -> Response<T>
    ): Data<T> {
        try {
            // Execute request
            val response = request()

            // Return success
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    return Data.success(it)
                }
            }

            // Retry request
            if (tries > 0) {
                delay(retryDelay)
                return executeRequest(tries - 1, retryDelay * 2, request)
            }

            // Return error
            val code = response.code()
            val error = errorHandler.getError(code)
            return Data.error(error)

        } catch (e: Exception) {
            // Retry request
            if (tries > 0) {
                delay(retryDelay)
                return executeRequest(tries - 1, retryDelay * 2, request)
            }

            // Return error
            val error = errorHandler.getError(e)
            return Data.error(error)
        }
    }

}