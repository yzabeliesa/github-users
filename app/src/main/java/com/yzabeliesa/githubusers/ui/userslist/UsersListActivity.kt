package com.yzabeliesa.githubusers.ui.userslist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.yzabeliesa.githubusers.R
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.ui.profile.ProfileActivity
import com.yzabeliesa.githubusers.util.ConnectivityWatcherAppCompatActivity
import com.yzabeliesa.githubusers.util.Error
import com.yzabeliesa.githubusers.util.OnItemClickListener
import com.yzabeliesa.githubusers.util.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_users_list.*

/**
 * This activity shows a scrollable list of Github users.
 */

@AndroidEntryPoint
class UsersListActivity : ConnectivityWatcherAppCompatActivity() {

    private lateinit var usersListAdapter: UsersListAdapter
    private lateinit var context: Context
    private lateinit var viewModel: UsersListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_list)

        context = this
        rootView = usersListRootView
        viewModel = ViewModelProvider(this).get(UsersListViewModel::class.java)

        setupRecyclerView()
        setupUsersListObserver()

        onAvailable = { viewModel.setLastUserId(0) }
    }

    /**
     * Sets up the @[RecyclerView] for showing users.
     */
    private fun setupRecyclerView() {
        usersListAdapter = UsersListAdapter(context)
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        usersListRvUsers.layoutManager = layoutManager
        usersListRvUsers.adapter = usersListAdapter

        // Handle user click event
        usersListAdapter.setOnItemClickListener(object : OnItemClickListener<User> {
            override fun onItemClick(item: User, position: Int) { onUserClicked(item, position) }
        })

        // Handle fetching new users upon scrolling to end of the RecyclerView
        usersListRvUsers.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                fetchNewUsersOnListEnd(recyclerView)
            }
        })
    }

    /**
     * Sets up the observer for updating the users list.
     */
    private fun setupUsersListObserver() {
        viewModel.getUsers().observe(this) { data ->
            when(data.status) {
                Status.LOADING -> {
                    if (snackbar?.isShown == true) snackbar?.dismiss()
                    usersListAdapter.showLoadingView()
                    data.value?.let { users ->
                        usersListAdapter.hideLoadingView()
                        usersListAdapter.setUsers(users)
                        usersListAdapter.showLoadingView()
                    }
                }

                Status.SUCCESS -> {
                    usersListAdapter.hideLoadingView()
                    data.value?.let { users ->
                        usersListAdapter.setUsers(users)
                    }
                    onAvailable = null
                }

                else -> {
                    usersListAdapter.hideLoadingView()
                    val message = when(data.error) {
                        Error.NO_INTERNET_CONNECTION -> getString(R.string.error_no_internet)
                        else -> getString(R.string.generic_error)
                    }
                    showErrorSnackbar(message, Snackbar.LENGTH_LONG)
                }
            }
        }
    }

    /**
     * Navigates to the profile screen upon clicking a user.
     */
    private fun onUserClicked(user: User, position: Int) {
        val intent = Intent(context, ProfileActivity::class.java)
        intent.putExtra("username", user.username)
        intent.putExtra("position", position)
        startActivity(intent)
    }

    /**
     * Fetches the next batch of users upon scrolling to the end of the list.
     */
    private fun fetchNewUsersOnListEnd(recyclerView: RecyclerView) {
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
        val lastVisible = layoutManager.findLastVisibleItemPosition()
        if (lastVisible > 0) {
            val totalItemCount = layoutManager.itemCount
            if (!usersListAdapter.isLoading() && totalItemCount == lastVisible + 1) {
                val lastUser = usersListAdapter.getUserAt(lastVisible)
                viewModel.setLastUserId(lastUser?.id ?: 0)
                onAvailable = { viewModel.setLastUserId(lastUser?.id ?: 0) }
            }
        }
    }

}