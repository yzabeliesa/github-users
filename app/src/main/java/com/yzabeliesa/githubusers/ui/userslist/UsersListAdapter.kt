package com.yzabeliesa.githubusers.ui.userslist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.yzabeliesa.githubusers.R
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.util.OnItemClickListener
import com.yzabeliesa.githubusers.util.invertDrawableColors
import kotlinx.android.synthetic.main.item_user.view.*

/**
 * The @[RecyclerView.Adapter] for displaying the list of users.
 */

private const val USER = 1
private const val LOADING = 2

class UsersListAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val usersList = ArrayList<User>()
    private var onItemClickListener: OnItemClickListener<User>? = null
    private var isLoading = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            USER -> UserViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_user, parent, false))
            else -> LoadingViewHolder(LayoutInflater.from(context)
                .inflate(R.layout.item_loading_spinner, parent, false))
        }
    }

    override fun getItemCount(): Int = this.usersList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = this.usersList[position]
        if (holder is UserViewHolder) {
            // Prevent inverted image view holder from being recycled
            if ((position + 1) % 4 == 0) holder.setIsRecyclable(false)

            holder.bind(item, position)
            holder.view.setOnClickListener(createClickListener(item, position))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (this.usersList[position].id != null) USER
        else LOADING
    }

    fun getUserAt(position: Int): User? {
        if (position >= this.usersList.size) return null
        return this.usersList[position]
    }

    /**
     * Shows loading indicator.
     */
    fun showLoadingView() {
        if (!isLoading) {
            this.usersList.add(User())
            notifyDataSetChanged()
        }
        this.isLoading = true
    }

    /**
     * Hides loading indicator.
     */
    fun hideLoadingView() {
        if (isLoading) {
            val lastIndex = this.usersList.size - 1
            val viewType = getItemViewType(lastIndex)
            if (viewType == LOADING) {
                this.usersList.removeAt(lastIndex)
                notifyDataSetChanged()
            }
        }
        this.isLoading = false
    }

    /**
     * Returns if the loading indicator is shown.
     */
    fun isLoading() = isLoading

    /**
     * Clears all users in current users list and sets a new users list.
     */
    fun setUsers(users: List<User>) {
        this.usersList.clear()
        this.usersList.addAll(users)
        notifyDataSetChanged()
    }

    /**
     * Sets the item click listener implementation.
     */
    fun setOnItemClickListener(listener: OnItemClickListener<User>) {
        this.onItemClickListener = listener
    }

    /**
     * Creates an item click listener for a user.
     */
    private fun createClickListener(user: User, position: Int): View.OnClickListener {
        return View.OnClickListener {
            this.onItemClickListener?.onItemClick(user, position)
        }
    }

    /**
     * View holder for Github user.
     */
    class UserViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun bind(user: User, position: Int) {

            // Load user details
            view.userTvUsername.text = user.username
            view.userTvUserType.text = user.type

            // Show note indicator if user has notes
            val shouldShowNote = if (user.note.isNullOrEmpty()) View.GONE else View.VISIBLE
            view.userNoteIndicator.visibility = shouldShowNote
            view.userIvNoteIcon.visibility = shouldShowNote

            // Load avatar
            user.avatarUrl?.let { url ->
                Glide.with(view.context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.color.user_avatar_border)
                    .error(R.drawable.ic_avatar_placeholder)
                    .into(view.userIvAvatar)
            }

            // Invert avatar colors at every 4th position
            if ((position + 1) % 4 == 0) view.userIvAvatar.invertDrawableColors()

        }
    }

    /**
     * View holder for loading indicator.
     */
    class LoadingViewHolder(view: View): RecyclerView.ViewHolder(view)

}