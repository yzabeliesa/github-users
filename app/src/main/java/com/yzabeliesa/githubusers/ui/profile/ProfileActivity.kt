package com.yzabeliesa.githubusers.ui.profile

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.snackbar.Snackbar
import com.yzabeliesa.githubusers.R
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.util.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.layout_profile_header.*
import kotlinx.android.synthetic.main.layout_profile_notes.*
import kotlinx.android.synthetic.main.layout_profile_stats.*
import kotlinx.android.synthetic.main.layout_profile_user_info.*

/**
 * This activity shows a selected Github user profile's details.
 */

@AndroidEntryPoint
class ProfileActivity : ConnectivityWatcherAppCompatActivity() {

    private lateinit var context: Context
    private lateinit var viewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        context = this
        rootView = profileRootView

        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        val username = intent.extras?.getString("username") ?: ""
        val position = intent.extras?.getInt("position") ?: 0
        initialize(username, position)

        viewModel.setUser(username)
        onAvailable = { viewModel.setUser(username) }
    }

    /**
     * Sets behavior when app bar home button is clicked.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return if (id == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    /**
     * Initializes screen UI elements.
     */
    private fun initialize(username: String, position: Int) {
        // Setup action bar
        profileCollapsingToolbarLayout.title = username
        setSupportActionBar(profileToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Setup save note button
        profileNotesBtnSaveNote.setOnClickListener { onSaveNoteClicked() }

        // Setup observers
        setupUserDetailsObserver(position)
    }

    /**
     * Sets up the observer for obtaining Github user profile's details based on the set user in
     * @[viewModel].
     * @param position Position of the user in the list.
     */
    private fun setupUserDetailsObserver(position: Int) {
        viewModel.getUserDetails().observe(this) { data ->
            when(data.status) {
                Status.LOADING -> {
                    setLoadingState(showProgress = true, showPage = false)
                    data.value?.let { user ->
                        bindUserToView(user, position)
                        setLoadingState(showProgress = true, showPage = true)
                    }
                }

                Status.SUCCESS -> {
                    data.value?.let { user ->
                        bindUserToView(user, position)
                    }
                    setLoadingState(showProgress = false, showPage = true)
                    onAvailable = null
                }

                else -> {
                    setLoadingState(showProgress = false,
                        showPage = profileIvAvatar.visibility == View.VISIBLE)
                    val message = when(data.error) {
                        Error.NO_INTERNET_CONNECTION -> getString(R.string.error_no_internet)
                        else -> getString(R.string.generic_error)
                    }
                    showErrorSnackbar(message, Snackbar.LENGTH_LONG)
                }
            }
        }
    }

    /**
     * Sets the loading state of the whole profile page.
     * @param showProgress If true, the progress bar is shown.
     * @param showPage If true, the profile page contents are shown.
     */
    private fun setLoadingState(showProgress: Boolean, showPage: Boolean) {
        val progressVisibility = if (showProgress) View.VISIBLE else View.GONE
        val pageVisibility = if (showPage) View.VISIBLE else View.GONE

        profileProgress.visibility = progressVisibility
        profileIvAvatar.visibility = pageVisibility
        profileTvUsername.visibility = pageVisibility
        profileTvBio.visibility = pageVisibility
        profileHeaderSpace.visibility = pageVisibility
        profileLayoutUserInfo.visibility = pageVisibility
        profileLayoutStats.visibility = pageVisibility
        profileLayoutNotes.visibility = pageVisibility
    }

    /**
     * Loads user details to the page.
     */
    private fun bindUserToView(user: User, position: Int) {
        profileTvUsername.text = user.username
        profileTvBio.setTextOrHideOnNullBlank(user.bio)
        loadProfilePicture(user.avatarUrl, position)
        loadUserInfo(user)
        loadStats(user)
        loadNotes(user)
    }

    /**
     * Loads the avatar of the user.
     */
    private fun loadProfilePicture(avatarUrl: String?, position: Int) {
        avatarUrl?.let { url ->
            Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.color.user_avatar_border)
                .error(R.drawable.ic_avatar_placeholder)
                .into(profileIvAvatar)

            if ((position + 1) % 4 == 0) profileIvAvatar.invertDrawableColors()
        }
    }

    /**
     * Loads user data shown in the User Info portion of the page.
     */
    private fun loadUserInfo(user: User) {
        profileUserInfoTvName.setTextOrHideOnNullBlank(user.name)
        profileUserInfoTvType.setTextOrHideOnNullBlank(user.type)
        profileUserInfoTvLocation.setTextOrHideOnNullBlank(user.location)
        profileUserInfoTvEmail.setTextOrHideOnNullBlank(user.email)
        profileUserInfoTvCompany.setTextOrHideOnNullBlank(user.company)
        profileUserInfoTvBlog.setTextOrHideOnNullBlank(user.blog)
        val createdAt = FormatUtil.formatDate(user.createdAt)?.let { "Member since $it" }
        profileUserInfoTvMemberSince.setTextOrHideOnNullBlank(createdAt)
    }

    /**
     * Loads user data shown in the Stats portion of the page.
     */
    private fun loadStats(user: User) {
        profileStatsPublicReposCount.text = FormatUtil.formatNumber(user.publicRepos ?: 0)
        profileStatsPublicGistsCount.text = FormatUtil.formatNumber(user.publicGists ?: 0)
        profileStatsFollowersCount.text = FormatUtil.formatNumber(user.followers ?: 0)
        profileStatsFollowingCount.text = FormatUtil.formatNumber(user.following ?: 0)
    }

    /**
     * Loads the saved user note, if any.
     */
    private fun loadNotes(user: User) {
        profileNotesEtNotes.setText(user.note ?: "")
    }

    /**
     * Saves the written note.
     */
    private fun onSaveNoteClicked() {
        val note = profileNotesEtNotes.text.toString()
        viewModel.saveNote(note)
        Toast.makeText(context, getString(R.string.note_saved), Toast.LENGTH_SHORT).show()
        profileNotesEtNotes.clearFocus()
    }

}