package com.yzabeliesa.githubusers.ui.profile

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.yzabeliesa.githubusers.data.model.Data
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.data.repository.UserRepository
import com.yzabeliesa.githubusers.util.Status
import kotlinx.coroutines.launch

/**
 * View model for Github user profile screen.
 */

class ProfileViewModel @ViewModelInject constructor (
    private val usersRepository: UserRepository
) : ViewModel() {

    private val usernameLiveData = MutableLiveData<String>()
    private val userDetailsLiveData = usernameLiveData.switchMap { username ->
        usersRepository.getUserDetails(username)
    }

    /**
     * Sets the user whose profile is shown.
     */
    fun setUser(username: String) {
        usernameLiveData.value = username
    }

    /**
     * Observable user details.
     */
    fun getUserDetails(): LiveData<Data<User>> {
        return userDetailsLiveData
    }

    /**
     * Saves a note for the user.
     */
    fun saveNote(note: String) {
        val userData = userDetailsLiveData.value
        if (userData?.status == Status.SUCCESS) {
            userData.value?.let { user ->
                viewModelScope.launch {
                    usersRepository.saveUserNote(user, note)
                }
            }
        }
    }

}