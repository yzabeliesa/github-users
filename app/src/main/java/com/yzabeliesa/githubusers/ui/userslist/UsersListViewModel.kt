package com.yzabeliesa.githubusers.ui.userslist

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.yzabeliesa.githubusers.data.model.Data
import com.yzabeliesa.githubusers.data.model.User
import com.yzabeliesa.githubusers.data.repository.UserRepository

/**
 * View model for Github users list screen.
 */

class UsersListViewModel @ViewModelInject constructor (
    private val usersRepository: UserRepository
) : ViewModel() {

    private val lastUserId = MutableLiveData(0)
    private val getUsersLiveData = lastUserId.switchMap { id -> usersRepository.getUsers(id) }

    /**
     * Sets the ID of the user that was last fetched.
     */
    fun setLastUserId(id: Int) {
        lastUserId.value = id
    }

    /**
     * Observable users list.
     */
    fun getUsers(): LiveData<Data<List<User>>> {
        return getUsersLiveData
    }

}