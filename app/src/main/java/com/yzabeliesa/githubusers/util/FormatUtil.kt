package com.yzabeliesa.githubusers.util

import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Utility class for displaying data in a certain format.
 */

object FormatUtil {

    /**
     * @param date Date in ISO-8601 format.
     * @return Date in month-year format.
     */
    fun formatDate(date: String?): String? {
        var formatted = date
        formatted?.let {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
            dateFormat.parse(it)?.let { parsed ->
                val displayFormat = SimpleDateFormat("MMM yyyy", Locale.getDefault())
                formatted = displayFormat.format(parsed)
            }
        }

        return formatted
    }

    /**
     * Displays large numbers into a shortened String representation. Numbers more than a million
     * are displayed with an 'M' suffix while numbers more than a thousand are displayed with a 'K'
     * suffix.
     */
    fun formatNumber(num: Long): String {
        if (num >= 1000000) {
            val millions = num.toDouble() / 1000000.0
            return DecimalFormat("#.##M").format(millions)
        }
        return if (num >= 1000) {
            val thousands = num.toDouble() / 1000.0
            DecimalFormat("#.#K").format(thousands)
        } else num.toString()
    }

}