package com.yzabeliesa.githubusers.util

import android.graphics.ColorMatrixColorFilter
import android.view.View
import android.widget.ImageView
import android.widget.TextView

/**
 * Extensions for @[View] objects.
 */

private val NEGATIVE = floatArrayOf(
    -1.0f, 0f, 0f, 0f, 255f,
    0f, -1.0f, 0f, 0f, 255f,
    0f, 0f, -1.0f, 0f, 255f,
    0f, 0f, 0f, 1.0f, 0f
)

/**
 * Inverts the image source of an @[ImageView].
 */
fun ImageView.invertDrawableColors() {
    colorFilter = ColorMatrixColorFilter(NEGATIVE)
}

/**
 * Sets the @[TextView] text or hides the TextView if the text data is null or blank.
 */
fun TextView.setTextOrHideOnNullBlank(str: String?) {
    if (str.isNullOrBlank()) visibility = View.GONE
    else {
        visibility = View.VISIBLE
        text = str
    }
}