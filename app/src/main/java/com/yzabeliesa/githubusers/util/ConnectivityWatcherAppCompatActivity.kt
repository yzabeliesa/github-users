package com.yzabeliesa.githubusers.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.yzabeliesa.githubusers.R

/**
 * An @[AppCompatActivity] that detects internet connectivity changes and performs the necessary
 * actions accordingly.
 */

open class ConnectivityWatcherAppCompatActivity : AppCompatActivity() {

    protected var snackbar: Snackbar? = null
    protected var rootView: View? = null

    protected var onAvailable: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

        // Register network callbacks
        connectivityManager?.let {
            // For API 24+
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                it.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        doOnAvailable()
                    }

                    override fun onLost(network: Network) {
                        showConnectionLostSnackbar()
                        super.onLost(network)
                    }
                })
            }

            // For API 23 and below
            else {
                val request = NetworkRequest.Builder().build()
                it.registerNetworkCallback(request, object: ConnectivityManager.NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        doOnAvailable()
                    }

                    override fun onLost(network: Network) {
                        showConnectionLostSnackbar()
                        super.onLost(network)
                    }
                })
            }
        }
    }

    /**
     * Dismisses the currently displayed error snackbar (if any) and shows a new error snackbar.
     */
    protected fun showErrorSnackbar(message: String, length: Int) {
        snackbar?.dismiss()
        if (snackbar == null) {
            rootView?.let {
                snackbar = Snackbar.make(it, message, length)
            }
        } else {
            snackbar?.apply {
                setText(message)
            }
        }
        snackbar?.show()
    }

    /**
     * Shows an error snackbar when internet connection is lost.
     */
    private fun showConnectionLostSnackbar() {
        showErrorSnackbar(getString(R.string.internet_connection_lost),
            Snackbar.LENGTH_INDEFINITE)
    }

    /**
     * Dismisses the error snackbar and runs @[onAvailable] upon detecting successful connection to
     * the internet.
     */
    private fun doOnAvailable() {
        snackbar?.dismiss()
        onAvailable?.let {
            runOnUiThread(it)
        }
    }

}