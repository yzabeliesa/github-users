package com.yzabeliesa.githubusers.util

/**
 * Generic click listener for selected item.
 */

interface OnItemClickListener<T> {
    fun onItemClick(item: T, position: Int)
}