package com.yzabeliesa.githubusers.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}