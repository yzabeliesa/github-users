package com.yzabeliesa.githubusers.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.yzabeliesa.githubusers.data.api.GithubApiService
import com.yzabeliesa.githubusers.data.api.RemoteUserDataSource
import com.yzabeliesa.githubusers.data.api.RemoteUserErrorHandler
import com.yzabeliesa.githubusers.data.db.GithubUsersDao
import com.yzabeliesa.githubusers.data.db.GithubUsersDatabase
import com.yzabeliesa.githubusers.data.db.LocalUserDataSource
import com.yzabeliesa.githubusers.data.prefs.LocalConfigDataSource
import com.yzabeliesa.githubusers.data.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Injects dependencies using Hilt.
 */

@Module
@InstallIn(ApplicationComponent::class)
object GithubUsersModule {

    /*
    ===========================
     Local config dependencies
    ===========================
    */
    @Singleton
    @Provides
    fun provideSharedPrefs(@ApplicationContext appContext: Context): SharedPreferences =
        appContext.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE)

    @Singleton
    @Provides
    fun provideLocalConfigDataSource(sharedPrefs: SharedPreferences) =
        LocalConfigDataSource(sharedPrefs)

    /*
    =========================
     Local user dependencies
    =========================
    */
    @Singleton
    @Provides
    fun provideGithubUsersDatabase(@ApplicationContext appContext: Context): GithubUsersDatabase =
        Room.databaseBuilder(appContext, GithubUsersDatabase::class.java, "users")
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun provideGithubUsersDao(db: GithubUsersDatabase) = db.usersDao()

    @Singleton
    @Provides
    fun provideLocalUserDataSource(dao: GithubUsersDao) = LocalUserDataSource(dao)

    /*
    ==========================
     Remote user dependencies
    ==========================
    */
    @Singleton
    @Provides
    fun provideRemoteUserErrorHandler() = RemoteUserErrorHandler()

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().setPrettyPrinting().create()

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)

    @Singleton
    @Provides
    fun provideRetrofit(gsonConverterFactory: GsonConverterFactory) : Retrofit = Retrofit.Builder()
        .baseUrl("https://api.github.com")
        .addConverterFactory(gsonConverterFactory)
        .build()

    @Singleton
    @Provides
    fun provideUsersApiService(retrofit: Retrofit): GithubApiService =
        retrofit.create(GithubApiService::class.java)

    @Singleton
    @Provides
    fun provideRemoteUserDataSource(apiService: GithubApiService,
                                    errorHandler: RemoteUserErrorHandler) =
        RemoteUserDataSource(apiService, errorHandler)

    /*
    ============
     Repository
    ============
    */
    @Singleton
    @Provides
    fun provideRepositoryCoroutineDispatcher() = Dispatchers.IO

    @Singleton
    @Provides
    fun provideUserRepository(remoteUserDataSource: RemoteUserDataSource,
                              localUserDataSource: LocalUserDataSource,
                              localConfigDataSource: LocalConfigDataSource,
                              coroutineDispatcher: CoroutineDispatcher) =
        UserRepository(
            remoteUserDataSource,
            localUserDataSource,
            localConfigDataSource,
            coroutineDispatcher)

}