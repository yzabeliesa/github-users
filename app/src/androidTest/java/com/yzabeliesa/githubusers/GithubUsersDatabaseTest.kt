package com.yzabeliesa.githubusers

import androidx.arch.core.executor.testing.CountingTaskExecutorRule
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.yzabeliesa.githubusers.data.db.GithubUsersDatabase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class GithubUsersDatabaseTest {

    @Rule
    @JvmField val countingTaskExecutorRule = CountingTaskExecutorRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: GithubUsersDatabase

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            GithubUsersDatabase::class.java
        ).build()
    }

    @After
    fun tearDown() {
        countingTaskExecutorRule.drainTasks(10, TimeUnit.SECONDS)
        database.close()
    }

    @Test
    fun testInsertUser(): Unit = runBlocking {
        val user = FAKE_USERS_LIST[0]
        database.usersDao().insertUser(user)

        val liveData = database.usersDao().getUser(user.username ?: "")
        liveData.observeForever {  }

        assert(liveData.value == user)
    }

    @Test
    fun testInsertUserOnConflict(): Unit = runBlocking {
        val user = FAKE_USERS_LIST[0]
        database.usersDao().insertUser(user)

        val observableUsers = database.usersDao().getAllUsers()
        observableUsers.observeForever {  }
        val observableUser = database.usersDao().getUser(user.username ?: "")
        observableUser.observeForever {  }
        val previousSize = observableUsers.value?.size ?: 0

        assert(observableUser.value?.name == user.name)

        val newName = "New Fakeuser"
        val modifiedUser = user.copy(name = newName)
        database.usersDao().insertUser(modifiedUser)

        val currentSize = observableUsers.value?.size ?: 0
        assert(previousSize == currentSize)
        assert(observableUser.value?.name == newName)
    }

    @Test
    fun testInsertUsers(): Unit = runBlocking {
        database.usersDao().insertUsers(FAKE_USERS_LIST)

        val liveData = database.usersDao().getAllUsers()
        liveData.observeForever {  }

        val users = liveData.value
        assert(users?.size == FAKE_USERS_LIST.size)
        users?.forEachIndexed { index, user ->
            assert(user == FAKE_USERS_LIST[index])
        }
    }

    @Test
    fun testInsertUsersOnConflict(): Unit = runBlocking {
        val oldName = "Old Fakeuser"
        val user = FAKE_USERS_LIST[0].copy(name = oldName)

        val usersLiveData = database.usersDao().getAllUsers()
        usersLiveData.observeForever {  }

        val userLiveDatabase = database.usersDao().getUser(user.username ?: "")
        userLiveDatabase.observeForever {  }
        assert(userLiveDatabase.value?.name == user.name)

        val users = FAKE_USERS_LIST
        database.usersDao().insertUsers(users)
        assert(usersLiveData.value?.size == users.size)
        assert(userLiveDatabase.value?.name != oldName)
    }

}