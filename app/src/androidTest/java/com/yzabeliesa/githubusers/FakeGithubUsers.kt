package com.yzabeliesa.githubusers

import com.yzabeliesa.githubusers.data.model.User

val FAKE_USERS_LIST: List<User> = listOf(
    User(id = 1, username = "fakeuser1", name = "Fake1 User1", type = "User"),
    User(id = 2, username = "fakeuser2", name = "Fake2 User2", type = "User"),
    User(id = 3, username = "fakeuser3", name = "Fake3 User3", type = "User"),
    User(id = 4, username = "fakeuser4", name = "Fake4 User4", type = "User"),
    User(id = 5, username = "fakeuser5", name = "Fake5 User5", type = "User"),
    User(id = 6, username = "fakeuser6", name = "Fake6 User6", type = "User")
)